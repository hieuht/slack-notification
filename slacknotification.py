#!/usr/bin/env python
#coding:utf-8
"""
  Author:  HieuHT --<>
  Purpose: 
  Created: 08/14/2015
"""
INCOMMING_WEBHOOKS="https://hooks.slack.com/services/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"
import urllib
import urllib2
import json
import sys
import optparse

def build_parser():
    usage = """
    usage: %prog -s 
    -s: service name
    """    
    parser = optparse.OptionParser(usage= usage)
    parser.add_option("-c", "--channel", dest="channel", help="Slack Channel.", default = "#general")    
    parser.add_option("-u", "--username", dest="username", help="Slack Username.")    
    parser.add_option("-n", "--name", dest="name", help="Service name.")
    parser.add_option("-i", "--ip", dest="ip", help="IP Address")
    parser.add_option("-s", "--server", dest="server", help="Server Name")
    parser.add_option("-f", "--failures", dest="failures", help="Number Failures")
    return parser

def req(message, channel, username):
    payload = {'text': message,
               'channel': channel,
               'username': username,
               'icon_emoji': ':mega:'
               }
    payload = json.dumps(payload)
    data = urllib.urlencode({'payload' : payload})
    request = urllib2.Request(INCOMMING_WEBHOOKS, data)
    try:        
        response = urllib2.urlopen(request)
        response_data = response.read()
        return response_data
    except Exception, e:
        print e

if __name__ == '__main__':
    parser = build_parser()
    options, _args = parser.parse_args()    
    messages = "*%s*: banned *%s* from *%s* after *%s* attempts" % (options.name, options.ip, options.server, options.failures)
    req(messages, options.channel, options.username)